package cat.dam.edgar.radiogroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{
    private ConstraintLayout mainLayout;
    private RadioGroup rg_textOptions, rg_mainOptions, rg_bckColor, rg_textColor, rg_textBckColor;
    private RadioButton rb_soSmall, rb_small, rb_medium, rb_big, rb_soBig, rb_textOptions;
    private RadioButton rb_backgroundRed, rb_backgroundGreen, rb_backgroundBlue, rb_backgroundWhite, rb_backgroundBlack;
    private TextView tv_size, tv_configure, tv_bckColor, tv_textColor, tv_textBckColor, tv_textColorName, tv_textBckColorName;
    private LinearLayout ll_resultText;
    private EditText et_bckColor, et_textBckColor, et_textColor;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
    }

    private void setVariables()
    {
        mainLayout = (ConstraintLayout) findViewById(R.id.cl_main);
        rg_mainOptions = (RadioGroup) findViewById(R.id.rg_mainOptions);
        setTextOptionsVar();
        setColorOptionsVar();
    }

    private void setTextOptionsVar()
    {
        tv_size = (TextView) findViewById(R.id.tv_size);
        tv_configure = (TextView) findViewById(R.id.tv_configure);
        rg_textOptions = (RadioGroup) findViewById(R.id.rg_textOptions);
        rb_soSmall = (RadioButton) findViewById(R.id.rb_soSmall);
        rb_small = (RadioButton) findViewById(R.id.rb_small);
        rb_medium = (RadioButton) findViewById(R.id.rb_medium);
        rb_big = (RadioButton) findViewById(R.id.rb_big);
        rb_soBig = (RadioButton) findViewById(R.id.rb_soBig);
        rb_textOptions = (RadioButton) findViewById(R.id.rb_textOptions);
        ll_resultText = (LinearLayout) findViewById(R.id.ll_resultText);
    }

    private void setColorOptionsVar()
    {
        colorGroupsVar();
        backgroundColorVar();
        editTextVar();
        textViewVar();
    }

    private void backgroundColorVar()
    {
        rb_backgroundBlack = (RadioButton) findViewById(R.id.rb_backgroundBlack);
        rb_backgroundRed = (RadioButton) findViewById(R.id.rb_backgroundRed);
        rb_backgroundBlue = (RadioButton) findViewById(R.id.rb_backgroundBlue);
        rb_backgroundGreen = (RadioButton) findViewById(R.id.rb_backgroundGreen);
        rb_backgroundWhite = (RadioButton) findViewById(R.id.rb_backgroundWhite);
    }

    private void editTextVar()
    {
        et_bckColor = (EditText) findViewById(R.id.et_bckColor);
        et_textColor = (EditText) findViewById(R.id.et_textColor);
        et_textBckColor = (EditText) findViewById(R.id.et_textBckColor);
    }

    private void textViewVar()
    {
        tv_bckColor = (TextView) findViewById(R.id.tv_bckColorName);
        tv_textColor = (TextView) findViewById(R.id.tv_textColor);
        tv_textBckColor = (TextView) findViewById(R.id.tv_textBckColor);
        tv_textColorName = (TextView) findViewById(R.id.tv_textColorName);
        tv_textBckColorName = (TextView) findViewById(R.id.tv_textBckColorName);
    }

    private void colorGroupsVar()
    {
        rg_textColor = (RadioGroup) findViewById(R.id.rg_textColor);
        rg_textBckColor = (RadioGroup) findViewById(R.id.rg_textBckColor);
        rg_bckColor = (RadioGroup) findViewById(R.id.rg_bckColor);
    }

    private void setListeners()
    {
        mainOptionListener();
        textListeners();
        backgroundColorListener();
        textColorListener();
        textBckListener();
        editBckListener();
        editTextColorListener();
        editTextBckColorListener();
    }

    private void mainOptionListener()
    {
        rg_mainOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == rb_textOptions.getId()){
                    setTextOptionsVisible(true);
                    setColorsOptionsVisible(false);
                } else {
                    setTextOptionsVisible(false);
                    setColorsOptionsVisible(true);
                }
            }
        });
    }

    private void setTextOptionsVisible(boolean visible)
    {
        if(visible){
            rg_textOptions.setVisibility(View.VISIBLE);
            ll_resultText.setVisibility(View.VISIBLE);
        } else {
            rg_textOptions.setVisibility(View.GONE);
            ll_resultText.setVisibility(View.GONE);
        }
    }

    private void setColorsOptionsVisible(boolean visible)
    {
        if(visible){
            rg_bckColor.setVisibility(View.VISIBLE);
            rg_textColor.setVisibility(View.VISIBLE);
            rg_textBckColor.setVisibility(View.VISIBLE);
        } else {
            rg_bckColor.setVisibility(View.GONE);
            rg_textColor.setVisibility(View.GONE);
            rg_textBckColor.setVisibility(View.GONE);
        }
    }

    private void textListeners()
    {
        rg_textOptions.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                String description = getResources().getString(R.string.size_text);
                switch (checkedId){
                    case R.id.rb_soSmall: tv_configure.setTextSize(10);
                        tv_size.setText(rb_soSmall.getText());
                        createToast(description, rb_soSmall.getText().toString());
                        break;
                    case R.id.rb_small: tv_configure.setTextSize(15);
                        tv_size.setText(rb_small.getText());
                        createToast(description, rb_small.getText().toString());
                        break;
                    case R.id.rb_big: tv_configure.setTextSize(25);
                        tv_size.setText(rb_big.getText());
                        createToast(description, rb_big.getText().toString());
                        break;
                    case R.id.rb_soBig: tv_configure.setTextSize(60);
                        tv_size.setText(rb_soBig.getText());
                        createToast(description, rb_soBig.getText().toString());
                        break;
                    default: tv_configure.setTextSize(20);
                        tv_size.setText(rb_medium.getText());
                        createToast(description, rb_medium.getText().toString());
                        break;
                }
            }
        });
    }

    private void backgroundColorListener()
    {
        rg_bckColor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String description = getResources().getString(R.string.bck_color);
                switch (checkedId){
                    case R.id.rb_backgroundRed: mainLayout.setBackgroundColor(getResources().getColor(R.color.colorRed));
                        tv_bckColor.setText(rb_backgroundRed.getText());
                        createToast(description, rb_backgroundRed.getText().toString());
                        break;
                    case R.id.rb_backgroundGreen: mainLayout.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                        tv_bckColor.setText(rb_backgroundGreen.getText());
                        createToast(description, rb_backgroundGreen.getText().toString());
                        break;
                    case R.id.rb_backgroundBlue: mainLayout.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                        tv_bckColor.setText(rb_backgroundBlue.getText());
                        createToast(description, rb_backgroundBlue.getText().toString());
                        break;
                    case R.id.rb_backgroundWhite: mainLayout.setBackgroundColor(getResources().getColor(R.color.white));
                        tv_bckColor.setText(rb_backgroundWhite.getText());
                        createToast(description, rb_backgroundWhite.getText().toString());
                        break;
                    case R.id.rb_backgroundBlack: mainLayout.setBackgroundColor(getResources().getColor(R.color.black));
                        tv_bckColor.setText(rb_backgroundBlack.getText());
                        createToast(description, rb_backgroundBlack.getText().toString());
                        break;
                }
            }
        });
    }

    private void textColorListener()
    {
        rg_textColor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String description = getResources().getString(R.string.text_color);
                switch (checkedId){
                    case R.id.rb_textColorRed: tv_configure.setTextColor(getResources().getColor(R.color.colorRed));
                        tv_textColor.setText(rb_backgroundRed.getText());
                        createToast(description, rb_backgroundRed.getText().toString());
                        break;
                    case R.id.rb_textColorGreen: tv_configure.setTextColor(getResources().getColor(R.color.colorGreen));
                        tv_textColor.setText(rb_backgroundGreen.getText());
                        createToast(description, rb_backgroundGreen.getText().toString());
                        break;
                    case R.id.rb_textColorBlue: tv_configure.setTextColor(getResources().getColor(R.color.colorBlue));
                        tv_textColor.setText(rb_backgroundBlue.getText());
                        createToast(description, rb_backgroundBlue.getText().toString());
                        break;
                    case R.id.rb_textColorWhite: tv_configure.setTextColor(getResources().getColor(R.color.white));
                        tv_textColor.setText(rb_backgroundWhite.getText());
                        createToast(description, rb_backgroundWhite.getText().toString());
                        break;
                    case R.id.rb_textColorBlack: tv_configure.setTextColor(getResources().getColor(R.color.black));
                        tv_textColor.setText(rb_backgroundBlack.getText());
                        createToast(description, rb_backgroundBlack.getText().toString());
                        break;
                }
            }
        });
    }

    private void textBckListener()
    {
        rg_textBckColor.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String description = getResources().getString(R.string.text_bck_color);
                switch (checkedId){
                    case R.id.rb_textBckColorRed: tv_configure.setBackgroundColor(getResources().getColor(R.color.colorRed));
                        tv_textBckColor.setText(rb_backgroundRed.getText());
                        createToast(description, rb_backgroundRed.getText().toString());
                        break;
                    case R.id.rb_textBckColorGreen: tv_configure.setBackgroundColor(getResources().getColor(R.color.colorGreen));
                        tv_textBckColor.setText(rb_backgroundGreen.getText());
                        createToast(description, rb_backgroundGreen.getText().toString());
                        break;
                    case R.id.rb_textBckColorBlue: tv_configure.setBackgroundColor(getResources().getColor(R.color.colorBlue));
                        tv_textBckColor.setText(rb_backgroundBlue.getText());
                        createToast(description, rb_backgroundBlue.getText().toString());
                        break;
                    case R.id.rb_textBckColorWhite: tv_configure.setBackgroundColor(getResources().getColor(R.color.white));
                        tv_textBckColor.setText(rb_backgroundWhite.getText());
                        createToast(description, rb_backgroundWhite.getText().toString());
                        break;
                    case R.id.rb_textBckColorBlack: tv_configure.setBackgroundColor(getResources().getColor(R.color.black));
                        tv_textBckColor.setText(rb_backgroundBlack.getText());
                        createToast(description, rb_backgroundBlack.getText().toString());
                        break;
                }
            }
        });
    }

    private void createToast(String description, String change)
    {
        String message;
        message = description +" "+change;
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void editBckListener()
    {
        et_bckColor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) rg_bckColor.clearCheck();
            }
        });

        et_bckColor.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String hexCode = et_bckColor.getText().toString();
                String description = getResources().getString(R.string.bck_color);

                if(checkHex(hexCode)){
                    String color = "#";
                    if(actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                        color += hexCode;
                        mainLayout.setBackgroundColor(Color.parseColor(color));
                        tv_bckColor.setText(color);
                        createToast(description, color);
                        return true;
                    }
                } else {
                    createToast(description, "Error");
                }

                return false;
            }
        });
    }

    private void editTextColorListener()
    {
        et_textColor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) rg_textColor.clearCheck();
            }
        });

        et_textColor.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String hexCode = et_textColor.getText().toString();
                String description = getResources().getString(R.string.text_color);

                if(checkHex(hexCode)){
                    String color = "#";
                    if(actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                        color += hexCode;
                        tv_configure.setTextColor(Color.parseColor(color));
                        tv_textColorName.setText(color);
                        createToast(description, color);
                        return true;
                    }
                } else {
                    createToast(description, "Error");
                }

                return false;
            }
        });
    }

    private void editTextBckColorListener()
    {
        et_textBckColor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) rg_textBckColor.clearCheck();
            }
        });

        et_textBckColor.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                String hexCode = et_textBckColor.getText().toString();
                String description = getResources().getString(R.string.text_bck_color);

                if(checkHex(hexCode)){
                    String color = "#";
                    if(actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER){
                        color += hexCode;
                        tv_configure.setBackgroundColor(Color.parseColor(color));
                        tv_textBckColorName.setText(color);
                        createToast(description, color);
                        return true;
                    }
                } else {
                    createToast(description, "Error");
                }

                return false;
            }
        });
    }

    private boolean checkHex(String hex)
    {
        boolean correct = true;
        int i = 0, character;
        hex = hex.toUpperCase();

        while (correct && i < hex.length()){
            character = (int) hex.charAt(i);
            if(!isNumber(character) && !isChar(character)) correct = false;
            else i++;
        }

        return correct;
    }

    private boolean isNumber(int n)
    {
        return (n < 58) && (n > 47);
    }

    private boolean isChar(int n)
    {
        return (n < 71) && (n > 64);
    }
}